

import { Fragment, useEffect, useState } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment' 



export default function BarChart({records}){

	const [months,setMonths] = useState([])
	const [monthlyAmount, setMonthlyAmount] = useState([])
	const [category, setCategory] = useState("")

	useEffect(()=>{
		
		if(records.length > 0) {

			let tempMonths = []

			records.forEach(element => {
				
				console.log(moment(element.date).format('MMMM'))

				if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){

					tempMonths.push(moment(element.date).format('MMMM'))
				}
			})

			console.log(tempMonths)
			
			const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			tempMonths.sort((a,b)=> {
				
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}
			})

			setMonths(tempMonths)

			
			if(records[0].categoryType==="Income"){

				setCategory("Income")

			} else if(records[0].categoryType==="Expense"){

				setCategory("Expense")
			}
		}

	},[records])


	useEffect(()=>{

		
		setMonthlyAmount(months.map(month => {
		
			let tempAmount = 0
			records.forEach(element => {

				if(moment(element.date).format('MMMM') === month){

					tempAmount = tempAmount + parseInt(element.amount)
				}

			})

			return tempAmount 

		}))

		
	},[months])

	let data
	
	if(category==="Income")
	{

		data = {

			labels: months,
			datasets: [
				{
					label: "Monthly Income * (On mobile, use landscape mode for better view)",
					backgroundColor: '#77dd77',
					borderColor: '#77dd77',
					borderWidth: 2,
					hoverBackgroundColor: '#3ace3a',
					hoverBorderColor: 'black',
					data: monthlyAmount
				}
			]			
		}

	} else {

		data = {

			labels: months,
			datasets: [
				{
					
					label: "Monthly Expense  * (On mobile, use landscape mode for better view)",
					backgroundColor: '#ff6961',
					borderColor: '#ff6961',
					borderWidth: 2,
					hoverBackgroundColor: '#ff392e',
					hoverBorderColor: 'black',
					data: monthlyAmount

				}
			]
		}
	}


	//start at 0
	const options = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            min: 0
          }    
        }]
      }
    };



	return(
		
		<Bar data={data}  options={options}/>
		
	)


}












	