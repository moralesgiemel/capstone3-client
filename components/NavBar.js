import { Fragment, useEffect, useState} from 'react'
import Link from 'next/link'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Router from 'next/router'
import AppHelper from '../app_helper.js'
//import Authenticate from './Authenticate'

export default function NavBar(){

	const [token, setToken] = useState('')

	

	/*useEffect(()=>{

    setToken(AppHelper.getAccessToken())

  	},[token])

 	console.log(token)*/

	//if user is not logged in redirect to login page
	useEffect(()=>{


		if(token==='ƒ' && (window.location.href !== "https://capstone-3-riotoc-git-master.jamilriotoc10.vercel.app/register" ||window.location.href !== "https://capstone-3-riotoc.vercel.app/register" || window.location.href !== "https://capstone-3-riotoc.jamilriotoc10.vercel.app/register" || window.location.href !== "http://localhost:3000/register")){

			Router.push('/')
		
		} 

		setToken(AppHelper.getAccessToken())
		

	},[token])
	

	function logout(){

		localStorage.clear()

	}

	return(

		//<Authenticate />

		<Navbar className="navbar-dark bg-dark navbar" expand="lg">
			<Link href="/home">
				<a className="navbar-brand">Budget Tracker</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">

					{

						token !== null
						?
						<Fragment>
							<Link href="/categories">
								<a className="nav-link" role="button">Categories</a>
							</Link>
							<Link href="/records">
								<a className="nav-link" role="button">Records</a>
							</Link>
							<Link href="/monthlyincome">
								<a className="nav-link" role="button">Monthly Income</a>
							</Link>							
							<Link href="/monthlyexpense">
								<a className="nav-link" role="button">Monthly Expense</a>
							</Link>
							<Link href="/trend">
								<a className="nav-link" role="button">Trend</a>
							</Link>
							<Link href="/about">
								<a className="nav-link" role="button">About</a>
							</Link>
						</Fragment>
						:
						<Fragment>
							<Link href="/">
								<a className="nav-link" role="button">Login</a>
							</Link>
							<Link href="/register">
								<a className="nav-link" role="button">Register</a>
							</Link>
						</Fragment>

							
					}
						


				</Nav>
				{
					token !== null
					?
					<Nav className="justify-content-end">
						<Link href="/" >
							<a className="nav-link" role="button" onClick={ e => logout() } >Logout</a>
						</Link> 
					</Nav>
					:
					null
				}
			</Navbar.Collapse>
		</Navbar>

	)

}

