

import { Fragment, useEffect, useState } from 'react'
import { Line } from 'react-chartjs-2'
import moment from 'moment' 



export default function LineChart({incomeRecords,expenseRecords}){

	const [months,setMonths] = useState([])
	const [incomeMonths, setIncomeMonths] = useState([])
	const [expenseMonths, setExpenseMonths] = useState([])
	const [incomeAmount, setIncomeAmount] = useState([])
	const [expenseAmount, setExpenseAmount] = useState([])
	

	//console.log(incomeRecords)
	//console.log(expenseRecords)

	//console.log(incomeRecords)
	//console.log(expenseRecords)

	useEffect(()=>{
		
		if(incomeRecords.length > 0) {

			let tempMonths = []

			incomeRecords.forEach(element => {
				
				//console.log(moment(element.date).format('MMMM'))

				if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){

					tempMonths.push(moment(element.date).format('MMMM'))
				}
			})

			//console.log(tempMonths)

			tempMonths.sort((a,b)=> {
				
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}

			})

			setIncomeMonths(tempMonths)

		}

		// -------------------------- For Expense ------------------------------

		if(expenseRecords.length > 0) {

			let tempMonths = []

			expenseRecords.forEach(element => {
				
				//console.log(moment(element.date).format('MMMM'))

				if(!tempMonths.find(month => month === moment(element.date).format('MMMM'))){

					tempMonths.push(moment(element.date).format('MMMM'))
				}
			})

			//console.log(tempMonths)
			
			const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

			tempMonths.sort((a,b)=> {
				
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}

			})

			setExpenseMonths(tempMonths)		
		}

	
	},[incomeRecords, expenseRecords])

	

	useEffect(()=>{

		const income = incomeMonths.map(month => {
		
			let tempIncomeAmount = 0
			incomeRecords.forEach(element => {


				if(moment(element.date).format('MMMM') === month){

					tempIncomeAmount = tempIncomeAmount + parseInt(element.amount)
				}

			})

			return tempIncomeAmount 

		})

		
		setIncomeAmount(income)
		
	},[incomeMonths])


	useEffect(()=>{

		
		const expense = expenseMonths.map(month => {
		
			let tempExpenseAmount = 0
			expenseRecords.forEach(element => {

				if(moment(element.date).format('MMMM') === month){

					tempExpenseAmount = tempExpenseAmount + parseInt(element.amount)
				}

			})

			return tempExpenseAmount 

		})


		setExpenseAmount(expense)

	},[expenseMonths])
		
		console.log(incomeMonths)
		console.log(expenseMonths)
		console.log(incomeAmount)
		console.log(expenseAmount)

	/*function finalBalance(){

		const finalBalance = incomeAmount.map(element => {

			return element - expenseAmount[indexOf(element)]

		})

		setBalance(balance)

	}

	finalBalance()

	console.log(balance)

*/
	let balance = []

	for(let i = 0; i < incomeAmount.length; i++){

		balance.push(incomeAmount[i]-expenseAmount[i])

	}

	console.log(balance)

	

	const monthsRef = ["January","February","March","April","May","June","July","August","September","October","November","December"]

	const data = {

			labels: incomeMonths,
			datasets: [
				{
					label: "Balance Trend * (On mobile, use landscape mode for better view)",
					backgroundColor: '#81dafc',
					borderColor: '#36c4fa',
					borderWidth: 3,
					hoverBackgroundColor: '#06a2de',
					hoverBorderColor: 'black',
					data: [...balance]
				}
			]			
	}

	const options = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            min: 0
          }    
        }]
      }
    };


	return(

		<Line data={data}  options={options}/>
	)
	
}	

	

	





	