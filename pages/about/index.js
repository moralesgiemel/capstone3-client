import { Fragment, useEffect,useState } from 'react'
import NavBar from '../../components/NavBar'
import Image from 'next/image'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Container from 'react-bootstrap/Container'
import {Row, Col} from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import AppHelper from '../../app_helper.js'
import Link from 'next/link'


export default function about(){

	const [token,setToken] = useState('')


	 useEffect(()=>{

    setToken(AppHelper.getAccessToken())

 	 },[])

	return (

		<Fragment>
			<NavBar />
			
			{
				token !==null
				?
				<Fragment>

					<Jumbotron className="text-center jumbotron">
				<h1>We're Developers, Gamers and Tech Enthusiasts</h1>
				<Button>Work with us!</Button>
				</Jumbotron>
				<Container>
					
				<Row className="mb-5">
					
					<Col className="image1">
					
						<img src="/images/profilepic-jam.jpg" alt="jam" className="jam mb-5"  />

						<blockquote class="blockquote mb-5 text-center">
						  <p class="mb-0">"A great professor once said: "Console log everything and thy problem shall be done".. Worked for me --- digitally."</p>
						  <footer class="blockquote-footer  mt-2">Jamil Riotoc, <cite title="Source Title">Full Stack Web Developer</cite></footer>
						</blockquote>

						<p className="mb-0"><strong>Email:</strong> jamil.riotoc@gmail.com</p>
						<p className="mb-0"><strong>LinkedIn:</strong> <Link href="https://www.linkedin.com/in/alfred-jamil-riotoc-9780a616b/"><a target="_blank">https://www.linkedin.com/in/alfred-jamil-riotoc-9780a616b/</a></Link></p>
						<p><strong>Contact:</strong> 0995 424 7623</p>
					
		    		</Col>
					
					
					<Col className="image2">
					
						<img src="/images/profilepic-gim.jpg" alt="gim" className="gim mb-5" />
					
						<blockquote class="blockquote mb-5 text-center">
						  <p class="mb-0">"A problem no matter how complex, could be solved when broken down into smaller fragments"</p>
						  <footer class="blockquote-footer mt-2">Giemel Morales, <cite title="Source Title">Full Stack Web Developer</cite></footer>
						</blockquote>

						<p className="mb-0"><strong>Email:</strong> moralesgiemel@gmail.com</p>
						<p className="mb-0"><strong>LinkedIn:</strong> <Link href="https://www.linkedin.com/in/giemel-jones-morales-b7b0721bb/"><a target="_blank">https://www.linkedin.com/in/giemel-jones-morales-b7b0721bb/</a></Link></p>
						<p><strong>Contact:</strong> 0915 521 6760</p>
						
					</Col>

				</Row>
				
				</Container>



				</Fragment>
				
				:
                <Fragment>
                <div className="errorPage">
                  <h5>403 Forbidden</h5>
                  
                  <Button variant="dark" href="/">Go to Login Page</Button>
                </div>
            
                </Fragment>

			}
			
			<footer className="footerBar">
				© Copyright {AppHelper.dateToday.getFullYear()} 
			</footer>	
		</Fragment>
	)


}



/*

	{
				token !==null
				?
				<Fragment>

				</Fragment>
				
				:
                <Fragment>
                <div className="errorPage">
                  <h5>403 Forbidden</h5>
                  
                  <Button variant="dark" href="/">Go to Home</Button>
                </div>
            
                </Fragment>

			}



*/