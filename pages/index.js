import { Fragment, useState, useEffect} from 'react'
import Image from 'next/image'
import Router from 'next/router'


import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Login.module.css'


import {GoogleLogin} from 'react-google-login'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'

import Swal from 'sweetalert2'

import AppHelper from '../app_helper.js'
const results = require('../results')

import NavBar from '../components/NavBar'


export default function login(){
 
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [token, setToken]  = useState(null)


  useEffect(()=>{

    setToken(AppHelper.getAccessToken())

  },[])


  const retrieveUserDetails = (accessToken) => {

		//add the URL to get the user detail but with the use of our App helper
		//http://localhost:4000/api
		const options = {

			headers: {

				Authorization: `Bearer ${accessToken}`
			}

		}

		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {

			Router.push('/home')

		})

	}


  function validate(e){
    
    e.preventDefault()
    
    const payload = {

      method: "POST",
      headers: {

    
        "Content-Type": "application/json"

      },
      body: JSON.stringify({

        email: email,
        password: password
      
      })

    }

    //verify if user is registered
    //if true redirect to profile, else show alert
    fetch(`${AppHelper.API_URL}/users/login`, payload)
    .then(AppHelper.toJSON)
    .then(data => {

      if(data){
        
        localStorage.setItem('token', data.accessToken)
        

        
        const token = localStorage.getItem('token')
        
      /*  console.log(token)*/

        const options = {

          headers: {

         
            Authorization: `Bearer ${token}`

          }

        }
       
        Swal.fire({ ...results.result.loginSuccess })
        Router.push('/home')

      } else {

        Swal.fire({ ...results.result.incorrect })

      } 

      
    }) //End of users/login fetch
    
  }

  function authenticateGoogleToken(response){

  /* console.log(response)*/
    
    //console.log(AppHelper.API_URL)//http://localhost:4000/api

    const payload = {

      method: "POST",
      headers: {

        'Content-Type': 'application/json'
      },
      body: JSON.stringify({

        tokenId: response.tokenId,
        accessToken: response.accessToken

      })

    }

    //http://localhost:4000/api
    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
    .then(AppHelper.toJSON)
    .then(data => {

   /*   console.log(data)*/
      //console.log(data)
      
      //to show alerts if the user logged in properly or there are errors.
      if(typeof data.accessToken !== 'undefined'){

        //set the accessToken into our localStorage
        localStorage.setItem('token', data.accessToken)
        //sweet alert to show user's succesful login
        Swal.fire({
          icon: 'success',
          title: "Successful Login"
        })
        //retrieve user details then redirect to courses
        retrieveUserDetails(data.accessToken)
      } else { //if data.accessToken is undefined

        if(data.error === 'google-auth-error'){
          Swal.fire({

            icon: 'error',
            title: 'Google Authentication Failed'

          })
        } else if(data.error === 'login-type-error'){

          Swal.fire({

            icon: 'error',
            title: 'You may have registered through a different login procedure.'

          })
        }

      }

    })
  }

  
 

/* console.log(token)*/

  return (
           
    <Fragment>
      <NavBar />
      
        {
            token === null
                ?
                 <Fragment>
                  <div className="loginContainer">
                  <h1 className="text-center">Budget Tracker</h1>
                  <Image
                    src="/images/budget-icon.png"
                    alt="Picture of the author"
                    width={50}
                    height={50}
                  />
                    <Form id="loginForm" onSubmit={e => validate(e)}>
                    <Form.Group controlId="userEmail">
                      <Form.Label>Email</Form.Label>
                      <Form.Control type="email" onChange={e => setEmail(e.target.value)} placeholder="Enter Email" required/>
                    </Form.Group>

                    <Form.Group controlId="password">
                      <Form.Label>Password</Form.Label>
                      <Form.Control type="password" onChange={e => setPassword(e.target.value)} placeholder="Enter Password" />
                    </Form.Group>
                    <Button variant="dark" type="submit" block>Login</Button>
                    <GoogleLogin 
                      clientId="146578207552-qmaav34o6s3k586s2nl1i9sfl080reg0.apps.googleusercontent.com"
                      buttonText="Login Using Google"
                      cookiePolicy={'single_host_origin'}
                      onSuccess={authenticateGoogleToken}
                      onFailure={authenticateGoogleToken}
                      className="w-100 text-center d-flex justify-content-center"
                    />

                    <div className="center-links">
                      <Nav.Link href="register">Sign Up</Nav.Link> <Nav.Link href="#login">Forgot password?</Nav.Link> 
                    </div>
                    
                    </Form>
                   </div>
                </Fragment>
              
                :
                <Fragment>
                <div className="errorPage">
                  <h5>403 Forbidden</h5>
                  
                  <Button variant="dark" href="/home">Go to HomePage</Button>
                </div>
            
                </Fragment>
                

        }
     
         
  
      
      <footer className="footerBar fixed-bottom">
        © Copyright {AppHelper.dateToday.getFullYear()} 
      </footer> 
    </Fragment>
         
  
  )


}
