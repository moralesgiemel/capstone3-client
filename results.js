

const result = {


	loginSuccess: {

		title: 'Login Successful',
		icon: 'success'
		
	},

	incorrect: {

		title: 'Ooops...',
		text: 'Incorrect email or password',
		icon: 'question',
		confirmButtonText: 'Try Again'
	},


	passwordMismatch: {
		
		title: 'Error!',
		text: 'The password that you entered did not match.',
		icon: 'error',
		confirmButtonText: 'Try Again'
	},

	emailExists: {
		
		title: 'Error!',
		text: 'The email that you entered is registered already.',
		icon: 'error',
		confirmButtonText: 'Try Again'
	},

	error: {

		title: 'Ooops!',
		text: 'Something went wrong...',
		icon: 'error',
		confirmButtonText: 'Try Again'
	}



}


module.exports.result = result