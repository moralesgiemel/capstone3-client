//process.env allows us to access the environment variable to be used in a file.


module.exports = {

	API_URL: process.env.NEXT_PUBLIC_API_URL,
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: res => res.json(),
	dateToday: new Date()
	
}